﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour
{

    public static Grid Instance;

	public bool onlyDisplayPathGizmos;
	public LayerMask unwalkableMask;
	private Vector2 gridWorldSize;
	private float nodeRadius;
	Node[,] grid;

	float nodeDiameter;
	int gridSizeX, gridSizeY;

    public List<Node> path;

    public Transform Roomba;

    private void Awake()
    {
        Instance = this;
    }

	public int MaxSize
    {
		get {
			return gridSizeX * gridSizeY;
		}
	}

	public void CreateGrid(int inHeight, int inWidth, Transform inObjectThatMap)
    {
        gridWorldSize = new Vector2(inWidth, inHeight);

        nodeDiameter = inObjectThatMap.localScale.x;

        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

        grid = new Node[gridSizeX,gridSizeY];
		Vector3 worldBottomLeft = inObjectThatMap.position - Vector3.right * gridWorldSize.x/2 - Vector3.forward * gridWorldSize.y/2;

		for (int x = 0; x < gridSizeX; x ++)
        {
			for (int y = 0; y < gridSizeY; y ++)
            {
				Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
				bool walkable = !(Physics.CheckSphere(worldPoint,nodeRadius,unwalkableMask));
				grid[x,y] = new Node(walkable,worldPoint, x,y);
			}
		}

        Roomba = inObjectThatMap;
	}

	public List<Node> GetNeighbours(Node node)
    {
		List<Node> neighbours = new List<Node>();

		for (int x = -1; x <= 1; x++)
        {
			for (int y = -1; y <= 1; y++)
            {
				if (x == 0 && y == 0)
					continue;

				int checkX = node.gridX + x;
				int checkY = node.gridY + y;

				if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
					neighbours.Add(grid[checkX,checkY]);
				}
			}
		}

		return neighbours;
	}
	

	public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
		float percentX = (worldPosition.x + gridWorldSize.x/2) / gridWorldSize.x;
		float percentY = (worldPosition.z + gridWorldSize.y/2) / gridWorldSize.y;
		percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);

		int x = Mathf.RoundToInt((gridSizeX) * percentX);
		int y = Mathf.RoundToInt((gridSizeY) * percentY);
		return grid[x,y];
	}

	
	void OnDrawGizmos()
    {
		Gizmos.DrawWireCube(transform.position,new Vector3(gridWorldSize.x,1,gridWorldSize.y));

		if (onlyDisplayPathGizmos)
        {
			if (path != null)
            {
				foreach (Node n in path)
                {
					Gizmos.color = Color.black;
					Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter-.1f));
				}
			}
		}
		else {

			if (grid != null)
            {
                Node roombaNode = NodeFromWorldPoint(Roomba.position);

                foreach (Node n in grid)
                {
					Gizmos.color = (n.walkable)?Color.white:Color.red;

					if (path != null)
						if (path.Contains(n))
							Gizmos.color = Color.black;

                    if (roombaNode.gridX == n.gridX && roombaNode.gridY == n.gridY)
                    {
                        Gizmos.color = Color.cyan;
                    }

                    Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter-.1f));
				}
			}
		}
	}
}