﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum DirectionState
{
    Left,
    Right,
    Forward,
    Back
}

public class RoombaBehaviour : MonoBehaviour
{
    public DirectionState CurrentDirection;

    Vector3 m_right = Vector3.right;
    Vector3 m_left = Vector3.left;
    Vector3 m_front = Vector3.forward;
    Vector3 m_back = Vector3.back;

    int m_rightRoomSize;
    int m_leftRoomSize;
    int m_frontRoomSize;
    int m_backRoomSize;

    int roomHeight;
    int roomWidth;

    public Transform target;
    public float speed = 5;
    private Vector3[] m_path;
    private int m_targetIndex;


    private void Start()
    {
        DetectRoom();

        CalculateRoomArea();

        Grid.Instance.CreateGrid(roomHeight,roomWidth, gameObject.transform);

        PathRequestManager.RequestPath(transform.position, target.position, OnPathFound);
    }

    private void Update()
    {
        Debug.Log(PathRequestManager.instance.isProcessingPath);
    }

    public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {
        if (pathSuccessful)
        {
            m_path = newPath;
            m_targetIndex = 0;
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }

    IEnumerator FollowPath()
    {
        Vector3 currentWaypoint = m_path[0];
        while (true)
        {
            if (transform.position == currentWaypoint)
            {
                m_targetIndex++;
                if (m_targetIndex >= m_path.Length)
                {
                    NewWallTarget(Vector3.right);
                    yield return new WaitForFixedUpdate();
                    PathRequestManager.RequestPath(transform.position, target.position, OnPathFound);
                }
                
                currentWaypoint = m_path[m_targetIndex];
            }

            transform.LookAt(currentWaypoint);
            transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);

            

            yield return null;
        }
    }

        

    public void NewWallTarget(Vector3 inDirection)
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.TransformDirection(inDirection), out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(inDirection) * hit.distance, Color.yellow);

            int directionDistance = Mathf.RoundToInt(hit.distance);

            if (inDirection == Vector3.left)
            {
                target.position = hit.point;
                CurrentDirection = DirectionState.Left;
            }
            else if (inDirection == Vector3.right)
            {
                target.position = hit.point;
                CurrentDirection = DirectionState.Right;
            }
            else if (inDirection == Vector3.forward)
            {
                target.position = hit.point;
                CurrentDirection = DirectionState.Forward;
            }
            else if (inDirection == Vector3.back)
            {
                target.position = hit.point;
                CurrentDirection = DirectionState.Back;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(inDirection) * 1000, Color.white);
        }
    }

    public void RoombaActions()
    {
        switch (CurrentDirection)
        {
            case DirectionState.Left:
                NewWallTarget(Vector3.right);
                break;

            case DirectionState.Right:
                NewWallTarget(Vector3.right);
                break;

            case DirectionState.Forward:
                break;

            case DirectionState.Back:
                break;
        }
    }

    #region Start Room Scan

    public void CalculateRoomArea()
    {
        if(m_rightRoomSize > m_leftRoomSize)
        {
            roomWidth = m_rightRoomSize * 2;
        }
        else
        {
            roomWidth = m_leftRoomSize * 2;
        }

        if(m_frontRoomSize > m_backRoomSize)
        {
            roomHeight = m_frontRoomSize * 2;
        }
        else
        {
            roomHeight = m_backRoomSize * 2;
        }
    }

    public void DetectRoom()
    {
        m_rightRoomSize = DetectDistanceRoomWalls(m_right);
        m_leftRoomSize = DetectDistanceRoomWalls(m_left);
        m_frontRoomSize = DetectDistanceRoomWalls(m_front);
        m_backRoomSize = DetectDistanceRoomWalls(m_back);

        
    }

    public int DetectDistanceRoomWalls(Vector3 direction)
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.TransformDirection(direction), out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(direction) * hit.distance, Color.yellow);

            int directionDistance = Mathf.RoundToInt(hit.distance);

            if(direction == Vector3.left)
            {
                target.position = hit.point;
                CurrentDirection = DirectionState.Left;
            }

            return directionDistance;
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(direction) * 1000, Color.white);

            return 0;
        }
    }
    #endregion

    public void OnDrawGizmos()
    {
        if (m_path != null)
        {
            for (int i = m_targetIndex; i < m_path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(m_path[i], transform.localScale);

                if (i == m_targetIndex)
                {
                    Gizmos.DrawLine(transform.position, m_path[i]);
                }
                else
                {
                    Gizmos.DrawLine(m_path[i - 1], m_path[i]);
                }
            }
        }
    }
}
