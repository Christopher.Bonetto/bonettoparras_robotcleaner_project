﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionAreaBehaviour : MonoBehaviour, ICanDetect
{
    /*summary
     * Method used to return a Collider.
     * it check if there is the interested layer intro a zone.
     * if it found something return how much colliders it has founded.
     * check them are into the customizable cone.
     * check if there is an obstacles between the StartDetectPoint to the Collider's point. It does this for each collider it has founded before.
     * after that check if the interested object has a RigidBody component.
     * if all of them return a value this method will return a collider. That will let to know his position or take his components.
     */
    public Collider DetectArea(Transform StartDetectPoint, float ViewRadius, float ViewAngle, Collider[] DetectedColliders, int NumberOfCollidersDetected, LayerMask InterestedLayerMask)
    {
        NumberOfCollidersDetected = Physics.OverlapSphereNonAlloc(StartDetectPoint.transform.position, ViewRadius, DetectedColliders, InterestedLayerMask);

        for (int i = 0; i < NumberOfCollidersDetected; i++)
        {
            if (DetectedColliders[i])
            {
                float angle = Vector3.Angle(StartDetectPoint.transform.forward, DetectedColliders[i].transform.position - StartDetectPoint.transform.position);

                RaycastHit hit;

                if (Mathf.Abs(angle) < ViewAngle / 2 && Physics.Raycast(StartDetectPoint.transform.position, DetectedColliders[i].transform.position - StartDetectPoint.transform.position, out hit, ViewRadius, InterestedLayerMask))
                {

                    if (hit.collider.GetComponent<Collider>())
                    {
                        Debug.DrawLine(StartDetectPoint.transform.position, DetectedColliders[i].transform.position, Color.red);

                        return DetectedColliders[i];
                    }
                }

            }
        }
        return null;
    }
}
