﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoombaStates
{
    Move,
    ChooseDirection,
    Spiral,
    Bounce
}

public class RoombaActions : MonoBehaviour
{
    #region Roomba state variables
    private RoombaStates m_CurrentState;
    public RoombaStates CurrentState
    {
        get
        {
            return m_CurrentState;
        }
        set
        {
            m_timer = 0;
            if (value == RoombaStates.ChooseDirection)
            {
                int randomDegrees = Random.Range(90, 270);
                RotationSystem(randomDegrees);
            }

            m_CurrentState = value;
            Debug.Log(m_CurrentState);
        }
    }

    private float m_timer = 0;
    [SerializeField] private float m_timerToChangeStateFrontal;

    [SerializeField] private bool m_wantSpiralStartingState = false;

    [Space,Space]
    #endregion

    #region Movements variables

    [SerializeField]private float speed = 5.0f;

    #region Circular variables
    [SerializeField]private float m_timerToChangeDegrees = 0;
    private Vector3 anglesToRotate;
    private Vector3 currentRotation;
    [SerializeField]private float m_circularDegrees = 2;
    private float m_currentCircularDegrees;
    #endregion

    #region Rotation variables

    [SerializeField] private float m_rotationSpeed = 0.5f;
    private Quaternion m_startingRotation;
    private Quaternion destinationRotation;
    private Vector3 nextPosition = Vector3.zero;

    private bool m_isRotating = false;
    private float m_progressRotation = 0;

    #endregion

    #endregion

    #region Detection Area variables
    public ICanDetect m_detectionInterface { get; private set; }

    [Range(0f, 360f), SerializeField]
    private float m_RoombaViewAngle = 100f;

    public float CurrentAngle
    {
        get
        {
            return Mathf.Clamp(m_RoombaViewAngle, 0, 360);
        }
        private set
        {
            m_RoombaViewAngle = value;
        }
    }

    [SerializeField]
    private float m_RoombaViewRadius = 0.7f;

    public float CurrentRadius
    {
        get
        {
            return Mathf.Clamp(m_RoombaViewRadius, 0, 20);
        }
        private set
        {
            m_RoombaViewRadius = value;
        }
    }

    [SerializeField]
    private LayerMask m_roombaDetectionMask;

    public Collider[] m_collidersDetected { get; private set; } = new Collider[1];
    private int m_numberOfColliderDetected;

    #endregion

    //-------------------

    #region Behaviour Cycle
    private void Awake()
    {
        //Initialize detection interface
        m_detectionInterface = new DetectionAreaBehaviour();
    }
    private void Start()
    {
        m_startingRotation = transform.rotation;
        m_currentCircularDegrees = m_circularDegrees;

        //Take a starting random state or not
        if (!m_wantSpiralStartingState)
        {
            ChangeRandomState();
        }
        else
        {
            CurrentState = RoombaStates.Spiral;
        }
    }
    void Update()
    {
        StatesSystem();
    }
    #endregion

    //-------------------

    #region Roomba state management
    private void StatesSystem()
    {
        switch (m_CurrentState)
        {
            //Roomba moves and checks frontal collisions, it considers if take another state during the movement too. 
            case RoombaStates.Move:
                Movement();
                CheckFrontCollision();
                CounterForNewState(m_timerToChangeStateFrontal);
                break;

            //It rotates to reaches the final rotation
            case RoombaStates.ChooseDirection:
                if (m_isRotating)
                {
                    ApplyRotation(destinationRotation);
                }
                break;

            //A change of rotation is applied to him as he goes straight. It checks frontal collisions too.
            case RoombaStates.Spiral:
                Movement();
                ApplyCircularMovement(m_currentCircularDegrees);
                CheckFrontCollision();
                ChangeSpriralDegrees();
                break;

            case RoombaStates.Bounce:
                break;
        }
    }

    private void CounterForNewState(float inMaxTime)
    {
        if (Timer(inMaxTime))
        {
            ChangeRandomState();
        }
    }

    private void ChangeRandomState()
    {
        int randomInt = Random.Range(0, 3);
        ChooseNewState(randomInt);
    }

    private void ChooseNewState(int inValue)
    {
        switch (inValue)
        {
            case 0:
                CurrentState = RoombaStates.Move;
                break;

            case 1:
                CurrentState = RoombaStates.ChooseDirection;
                break;

            case 2:
                CurrentState = RoombaStates.Spiral;
                break;
        }
    }

    #endregion


    #region Movements and Rotation

    private void Movement()
    {
        transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
    }

    private void ApplyCircularMovement(float yAxis = 0, float xAxis = 0, float zAxis = 0)
    {
        anglesToRotate.y = yAxis;
        anglesToRotate.x = xAxis;
        anglesToRotate.z = zAxis;

        //Take X,Y and Z angle values and store them to three different Quaternion. To avoid Gimbal Lock
        Quaternion rotationY = Quaternion.AngleAxis(anglesToRotate.y, new Vector3(0f, 1f, 0f));
        Quaternion rotationX = Quaternion.AngleAxis(anglesToRotate.x, new Vector3(1f, 0f, 0f));
        Quaternion rotationZ = Quaternion.AngleAxis(anglesToRotate.z, new Vector3(0f, 0f, 1f));

        //Then it is applied the transformed rotation to the Roomba.
        this.transform.rotation = this.transform.rotation * rotationY * rotationX * rotationZ;

        //Finally it is stored the current rotation.
        currentRotation = currentRotation + anglesToRotate * Time.deltaTime;
        currentRotation = new Vector3(currentRotation.x % 360f, currentRotation.y % 360f, currentRotation.z % 360f);
    }

    private void ChangeSpriralDegrees()
    {
        //Used to change the amplitude
        if (Timer(m_timerToChangeDegrees))
        {
            m_currentCircularDegrees -= 0.5f;
        }

        //Used to prevent degrees from becoming less than zero
        if (m_currentCircularDegrees <= 0)
        {
            m_currentCircularDegrees = m_circularDegrees;

            CurrentState = RoombaStates.Move;
        }
    }


    #region Rotate

    private void RotationSystem(float degreesToRotate)
    {
        if (!m_isRotating)
        {
            //Initialize the rotation. It set the destination rotation used to the Changing direction state.
            m_isRotating = true;
            nextPosition.y = m_startingRotation.eulerAngles.y + degreesToRotate;
            destinationRotation = Quaternion.Euler(nextPosition);
        }
    }

    private void ApplyRotation(Quaternion desideredRotation)
    {
        //Value clamped and used to the rotation's progression.
        m_progressRotation += m_rotationSpeed * Time.deltaTime;
        m_progressRotation = Mathf.Clamp01(m_progressRotation);

        //A slerp applied to the roomba's rotation.
        transform.rotation = Quaternion.Slerp(m_startingRotation, desideredRotation, m_progressRotation);

        //When the progression reaches 1 (because was clamped) means that the rotation is over.
        if (m_progressRotation == 1)
        {
            m_startingRotation = transform.rotation;
            m_isRotating = false;
            m_progressRotation = 0;

            CurrentState = RoombaStates.Move;
        }
    }
    #endregion

    #endregion

    private void CheckFrontCollision()
    {
        if (m_detectionInterface.DetectArea(gameObject.transform, m_RoombaViewRadius, m_RoombaViewAngle, m_collidersDetected, m_numberOfColliderDetected, m_roombaDetectionMask) != null)
        {
            CurrentState = RoombaStates.ChooseDirection;
        }
    }

    private bool Timer(float destinationTime)
    {
        m_timer += Time.deltaTime;

        if (m_timer >= destinationTime)
        {
            m_timer = 0;
            return true;
        }
        else
        {
            return false;
        }
    }

    #region DetectionGizmo
    //Gizmo too see the OverlapSphere

#if UNITY_EDITOR

    public void OnDrawGizmosSelected()
        {
            EditorGizmo(transform);
        }

        public void EditorGizmo(Transform transform)
        {
            ViewGizmo(Color.green, m_RoombaViewAngle, m_RoombaViewRadius);
        }

        private void ViewGizmo(Color color, float angle, float radius)
        {
            Color c = color;
            UnityEditor.Handles.color = c;

            Vector3 rotatedForward = Quaternion.Euler(0, -angle * 0.5f, 0) * transform.forward;

            UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, rotatedForward, angle, radius);
        }
    #endif

    #endregion
}
