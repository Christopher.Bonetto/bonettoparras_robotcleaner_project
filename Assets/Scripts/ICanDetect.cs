﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICanDetect
{
    Collider DetectArea(Transform startDetectPoint, float ViewRadius, float ViewAngle, Collider[] detectedColliders, int numberOfCollidersDetected, LayerMask interestedLayerMask);
}
