﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to the frontal part of the roomba
/// and make sure it doesn't keep bumping with the wall.
/// </summary>
public class FrontalCollisionDetection : MonoBehaviour
{
    private RoombaActions m_roomba;
    private float m_timer;

    #region Behaviour Cycle
    private void Awake()
    {
        m_roomba = gameObject.GetComponentInParent<RoombaActions>();
    }

    void Update()
    {
        transform.forward = m_roomba.transform.forward;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.name != m_roomba.transform.name)
        {
            if (Timer(0.1f))
            {
                m_roomba.CurrentState = RoombaStates.ChooseDirection;
            }
        }
    }
    #endregion

    #region Timer
    private bool Timer(float destinationTime)
    {
        m_timer += Time.deltaTime;

        if (m_timer >= destinationTime)
        {
            m_timer = 0f;

            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion
}
